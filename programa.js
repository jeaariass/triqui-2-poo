var turno = "X";
var miMatriz = [
    [" ", " ", " "],
    [" ", " ", " "],
    [" ", " ", " "]
];

//punto de entrada a el juego
function iniciarJuego() {
    let miBoton;
    let formulario;
    let salto;
    let alternar;
    formulario = document.getElementById("formTablero");
    for (let i = 1; i < 10; i++) {
        miBoton = document.createElement("input");
        // para atributos se recomienda el setAttribute 
        miBoton.setAttribute("type", "button");
        //miBoton.type = "button"; es igual que la linea anterior
        miBoton.setAttribute("id", "boton" + i);
        miBoton.setAttribute("class", "boton");
        miBoton.setAttribute("value", " ");
        //    miBoton.setAttribute("onclick", "realizarJugada(this)");
        // solo puede hacer una accion :c
        miBoton.setAttribute("onclick", "alternar(this)");
        formulario.appendChild(miBoton); //appendChild llama a la funcion para agregar un elemento 
        if (i % 3 == 0) {
            salto = document.createElement("br");
            formulario.appendChild(salto);
        }
    }

    for (let i = 1; i < 3; i++) {
        alternar = document.createElement("input");
        alternar.setAttribute("type", "button");
        alternar.setAttribute("id", "cambiar" + i);
        alternar.setAttribute("class", "cambiar");
        //  alternar.setAttribute("onclick", "alternar()")
        formulario.appendChild(alternar);
    }
}

function alternar(elemento) {
    let boton1 = document.getElementById('cambiar1');
    let boton2 = document.getElementById('cambiar2');

    if (empate() == false) {
        if (elemento.value == " ") {

            if (boton1.disabled === true) {
                boton1.disabled = false;
                boton2.disabled = true;
                realizarJugada(elemento);

            } else {

                boton1.disabled = true;
                boton2.disabled = false;
                realizarJugada(elemento);
            }
        } else {
            alert("este espacio esta ocupado :c ");
        }
    } else {
        alert("Empataron :c Sorry vuelve a intentarlo ");
        borrarTablero();
    }
}


function realizarJugada(elemento) {
    //  alert(elemento.id);
    elemento.value = turno;

    if (turno === "X") {
        turno = "O";
    } else {
        turno = "X";
    }


    let pos = 1;
    for (let i = 0; i < 3; i++) {
        for (let j = 0; j < 3; j++) {
            miMatriz[i][j] = document.getElementById(`boton` + pos).value;
            pos++;
        }
    };
    if (verificartriqui() === true) {
        alert("ganaste felicidades");
        borrarTablero();
    }
}

function verificartriqui() {

    let triqui = false;
    // SE CREARON DOS IF POR CADA DIAGONAL PARA MIRAR SI HAY UN TRIQUI CON X O CON O
    //Verificar si hay triqui en las filas
    for (let i = 0; i < 3; i++) {
        if (miMatriz[i][0] === 'O' && miMatriz[i][1] === 'O' && miMatriz[i][2] === 'O') {
            triqui = true;
        }
        if (miMatriz[i][0] === 'X' && miMatriz[i][1] === 'X' && miMatriz[i][2] === 'X') {
            triqui = true;
        }
    }

    //Verificar si hay triqui en las columnas
    for (let j = 0; j < 3; j++) {
        if (miMatriz[0][j] === 'O' && miMatriz[1][j] === 'O' && miMatriz[2][j] === 'O') {
            triqui = true;
        }
        if (miMatriz[0][j] === 'X' && miMatriz[1][j] === 'X' && miMatriz[2][j] === 'X') {
            triqui = true;
        }
    }

    // Para verificar si hay triqui en las diagonales
    if (miMatriz[0][0] === 'O' && miMatriz[1][1] === 'O' && miMatriz[2][2] === 'O') {
        triqui = true;
    }
    if (miMatriz[0][0] === 'X' && miMatriz[1][1] === 'X' && miMatriz[2][2] === 'X') {
        triqui = true;
    }

    if (miMatriz[0][2] === 'O' && miMatriz[1][1] === 'O' && miMatriz[2][0] === 'O') {
        triqui = true;
    }

    if (miMatriz[0][2] === 'X' && miMatriz[1][1] === 'X' && miMatriz[2][0] === 'X') {
        triqui = true;
    }

    return triqui;

}

function borrarTablero() {
    let miBoton;
    turno = "X";
    for (i = 1; i < 10; i++) {
        miBoton = document.getElementById("boton" + i);
        miBoton.value = " ";
    }

}

function empate() {
    let empate = 0;
    let sol = false;
    for (let i = 0; i < 3; i++) {
        for (let j = 0; j < 3; j++) {
            if (miMatriz[i][j] != " ") {
                empate++;
            }
        }
    }
    if (empate == 9) {
        sol = true;
    }
    return sol;

}